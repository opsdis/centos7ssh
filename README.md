# dockerfiles-centos-ssh

This Docker image is a very empty Centos 7 distribution.
The purpose is to use it to test Ansible playbooks in a simple way.


> Using docker arguments requiere docker 1.9 or higher

# Building
Copy an `authorized_keys` files that include the public ssh key of the user
you want to use to ssh into the container.

Copy the sources to your Docker host and build the container:

	$ docker build --build-arg username=<username> --rm -t <username>/ssh:centos7 .


# Running
To run with dedicated port:

	$ docker run -d -p 12022:22 <username>/ssh:centos7

> Port 12022 can be switched to some other port of your choice

To run with random port:

Get the port that the container is listening on:

	$ docker run -d -p 22 <username>/ssh:centos7


```
# docker ps
CONTAINER ID        IMAGE                 COMMAND             CREATED             STATUS              PORTS                   NAMES
8c82a9287b23        <username>/ssh:centos7   /usr/sbin/sshd -D   4 seconds ago       Up 2 seconds        0.0.0.0:49154->22/tcp   mad_mccarthy
```


To test, use the port that was selected or was just located:

	$ ssh -p xxxx <username>@localhost 


# Test Ansible
> Ansible must be installed on your local host

Get Ansible facts for the host `test1` in the inventory file `inventory`:
    
    $ ansible -i inventory test1 -m setup

> The `test1` is localhost at ssh port 12022

