#!/bin/bash

__create_user() {
# Create a user to SSH into as.
username=$1
useradd $username
SSH_USERPASS=newpass
echo -e "$SSH_USERPASS\n$SSH_USERPASS" | (passwd --stdin $username)
mkdir /home/$username/.ssh
chmod 700 /home/$username/.ssh
cp authorized_keys /home/$username/.ssh
chmod 600 /home/$username/.ssh/authorized_keys
chown -R $username:$username /home/$username/.ssh
usermod -aG wheel $username
}

# Call all functions
__create_user $1
