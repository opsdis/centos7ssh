# "ported" by Adam Miller <maxamillion@fedoraproject.org> from
#   https://github.com/fedora-cloud/Fedora-Dockerfiles
#
# Originally written for Fedora-Dockerfiles by
#   scollier <scollier@redhat.com>

FROM centos:7
MAINTAINER Opsdis <info@opsdis.com>
ARG username
RUN yum -y update; yum clean all
RUN yum -y install openssh-server passwd python3; yum clean all
RUN yum -y install sudo; yum clean all
ADD ./start.sh /start.sh
ADD ./authorized_keys /authorized_keys
ADD ./sudoers /etc/sudoers
RUN chmod 660 /etc/sudoers
RUN chown root:root /etc/sudoers
RUN mkdir /var/run/sshd

RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N '' 

RUN chmod 755 /start.sh
# EXPOSE 22
RUN ./start.sh $username
ENTRYPOINT ["/usr/sbin/sshd", "-D"]
